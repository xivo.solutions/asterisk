# asterisk

This repository contains the packaging information and patches for [Asterisk](http://www.asterisk.org/).

This repository is organized as follows:
- `master`: it contains the asterisk 22 patches for our debian12 based LTS (Orion+)
- `debian12_ast20`: it contains the asterisk 20 patches for our debian12 based LTS (Maia & Naos)
- `debian11_ast20_luna`: it contains the asterisk 20 patches for LTS Luna
- `debian11_ast20_kuma`: it contains the asterisk 20 patches for LTS Kuma
  - Note: this branch is only needed because we changed the asterisk service file following agid and confgend dockerization in Luna (that's why we were forced to create a branch for Luna)
- `debian11_ast18`: it contains the asterisk 18 patches for our debian11 based version (Izar & Jabbah)
- `debian10`: it contains the asterisk 18 patches for our debian10 based LTS (Gaia & Helios)


## To update the version of Asterisk:

- Update the version number in debian/rules
- Update the version number in debian/changelog by adding a new section
- Update the version number in Dockerfile
- In debuilder download the upstream tarball, extract it and cd into the directory

  ```bash
    debian/rules get-orig-source
    tar zxvf asterisk_18.2.0.orig.tar.gz
    cd asterisk-18.2.0
  ```

- Create a symbolic link to debian/patches, e.g. `ln -s ../debian/patches patches`

  ```bash
    ln -s ../debian/patches patches
  ```

- Update all the patches

  ```bash
    while quilt push;
    do quilt refresh -p ab;
    done
  ```

  - If a patch fail handle the conflict manually see [quilt update section](https://www.debian.org/doc/manuals/maint-guide/update.en.html#newupstream)
  - Then do a `quilt refresh -p ab`

- Commit and push

- If you need to stop at a specific patch :

  ```bash
    while quilt push; do
      # Vérifie si le patch actuel est "xivo_wrapup_termination"
      if [ "$(quilt top)" = "patches/xivo_wrapup_termination" ]; then
          echo "Arrêt : Patch xivo_wrapup_termination atteint."
          break
      fi
      # Actualise le patch
      quilt refresh -p ab
    done
  ```


### Quilt docs

- https://www.debian.org/doc/manuals/maint-guide/update.en.html#newupstream
- https://wiki.debian.org/UsingQuilt
- https://linux.die.net/man/1/quilt

## Build with jenkins

- Use the jenkins jobs to build the asterisk version:

  - [Debian 11](http://192.168.215.12:8080/job/asterisk-debian11/)
  - [Debian 12](http://192.168.215.12:8080/job/asterisk-debian12/)

- Once the build is successfull, on the remote machine add a source:

```bash
echo "deb http://mirror.xivo.solutions/debian/ asterisk-deb11 main" | sudo tee /etc/apt/sources.list.d/xivo-asterisk.list

#or
echo "deb http://mirror.xivo.solutions/debian/ asterisk-deb12 main" | sudo tee /etc/apt/sources.list.d/xivo-asterisk.list
```

Connect to the remote build machine:

```bash
apt update
apt install asterisk
```

## Tests

- Try these commands:

```bash
asterisk -r
module unload chan_sccp
module load chan_sccp
module unload res_freeze_check
module load res_freeze_check
dahdi show status
module reload chan_dahdi
```

- Make some call:

* A -> B
* A -> B => transfer to C
* ...

## Debug

It mioght be useful to debug and compare between asterisk with xivo patches and asterisk vanilla.
This is the purpose of Dockerfile-debug.
This dockerfile creates an image:
- with asterisk vanilla sources compiled
- and, in another directory, the asterisk with xivo patches compiled
Then you can run either the asterisk vanilla or the asterisk xivo if you launch the container with "xivo" in first parameter.

1. First, go to the debug dir:
   ```
   cd debug
   ```
2. build the container for a given asterisk version:
   ```
   ./docker_build.sh 22.2.0
   ```
3. When done you can launch the container:
   - Either with vanilla asterisk:
     ```
     ./run_docker.sh 22.2.0 /var/tmp/asteriskconf/ vanilla
     ```
   - Or with xivo asterisk:
     ```
     ./run_docker.sh 22.2.0 /var/tmp/asteriskconf/ xivo
     ```

**N.B.** The second parameter of the `./run_docker.sh` script is the full path toward a directory containing an asterisk configuration : it will be mounted in `/etc/asterisk/` inside the container.
