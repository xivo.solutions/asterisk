# Patches

Description of the patches on our asterisk package (`debian/patches`).
Listed in the order of the `debian/patches/series` file.

## deb_astgenkey-security

astgenkey should generate a private key that is not world-readable


## deb_make-clean-fixes

Don't remove sounds on dist-clean


## xivo_queue_agent_talking

Adding agent in conversation counter in QueueSummary AMI event (based on AST_DEVICE_INUSE flag)


## xivo_banner

Modifying banner message on Asterisk CLI


## xivo_skill_queues

Skill routing implementation

Reference :
- Upstream issue: [ASTERISK-17366](https://issues.asterisk.org/jira/browse/ASTERISK-17366)


## xivo_channel_check_lock

> Used by xivo-res-freeze-check
  Used by the reworked res_freeze_check module

*Note that the mutex is destroyed when the channels container is destroyed
(during asterisk shutdown), but we have no way to safely get a reference to the
channels container to make sure it isn't destroyed while we still have a reference
on its mutex, so we ignore this whole situation completely and suppose the returned
pointer is always valid.*

References:
- Original issue: [XiVO 5299](https://projects.wazo.community/issues/5299)


## xivo_senddigit_begin

Asterisk sometimes segfault when a DTMF is received at the same time an agent answers.
Note that patch does not completely solve the problem but reduces a lot the window when it can occurs.

References:
- Original issue: [XiVO 5114](https://projects.wazo.community/issues/5114)
- Upstream issue: [ASTERISK-21316](https://issues.asterisk.org/jira/browse/ASTERISK-21316)


## xivo_cel_event_hold

Add events HOLD and UNHOLD for XiVO CC stats

References:
- Original issue: [XiVO 4943](https://projects.wazo.community/issues/4943)


## xivo_queue_update_queue_before_agent_complete

app_queue: call update_queue before send_agent_complete

The base problem is that when a call between a caller and a queue member
is ended (either because of a hang up or transfer), the first
QueueMemberStatus AMI event that will be eventually sent will not have
its "LastCall" and "CallsTaken" value updated. An AgentComplete AMI
event will also be sent. In xivo-ctid, when we receive the AgentComplete
event, we ask for the queue member status of that member to get the
updated values. But in somes cases, the queue member state still hasn't
been updated.

With this patch, we make sure the queue member is updated *before* the
AgentComplete event is sent.


## xivo_queue_digits_gender

Add patch for digits gender in app_queue
Correctly say "1 minutes" in french ("*une* minutes" instead of "un minutes")

References:
- Original issue: [XiVO 4970](https://projects.wazo.community/issues/4970)
- Upstream issue: [ASTERISK-17088](https://issues.asterisk.org/jira/browse/ASTERISK-17088)
- Commit in project: cc6c2e7c


## xivo_dtmf_workaround_when_no_rtp

Fix related to DTMF via SIP info and directmedia.
Don't manipulate potentially destroyed channel

References:
- Original issue: [XiVO 5692](https://projects.wazo.community/issues/5692)
- Upstream issue: [ASTERISK-25214](https://issues.asterisk.org/jira/browse/ASTERISK-25214)
- Commit in project: 7e145b5d


## xivo_nova_compatibility

Add several information in CEL to enable compatibility with NOVA (SLIT) taxation system.
It needs the nova_compatibility=yes in the [general] section of /etc/asterisk/cel.conf file.

References:
- Original issue:
- Upstream issue: none
- Commits in project:
 - a213562e Fix leak in nova patch
 - a72b0657
 - aa31e9b0
 - 5cd4257e


## xivo_ast_tls_cert_fixed_pass

Simplify configuration of WebRTC lines in XiVO PBX

References:
- Original issue: [XiVO 522](https://projects.xivo.solutions/issues/522)
- Upstream issue: none


## xivo_max_dtmf_length

Fixes a 100% CPU if receiving a DTMF with enormous duration.

References:
- Original issue: [XiVO 795](https://projects.xivo.solutions/issues/795)
- Upstream issue: none


## xivo_queue_log_attended_transfer

Fixes a crash when doing an attended transfer towards a Queue via a Local channel.

References:
- Original issue: [XiVO 1063](https://projects.xivo.solutions/issues/1063)
- Upstream issue: none


## xivo_queue_handle_hangup

Fixes an asterisk crash when transfering a call that was redirected to a queue (via Redirect command)

References:
- Original issue: [XiVO 1277](https://projects.xivo.solutions/issues/1277)
- Upstream issue: [ASTERISK-27006](https://issues.asterisk.org/jira/browse/ASTERISK-27006)

## xivo_wrapup_termination

(Previous xivo_agent_complete_wrapup patch was merged in it)

- In the queue logs, the WRAPUPSTART event is associated to an agent but not to a queue. Having the information about the queue would allow to calculate more precise statistics about the wrapup.
- Feature to add the possibility to end the wrapup before its configured end.

References:
- Original issues
 - [XiVO 4937](https://projects.wazo.community/issues/4937)
 - [XiVO 5262](https://projects.wazo.community/issues/5262)
 - [XiVO 1793](https://projects.xivo.solutions/issues/1793)
- Original issue: [XiVO 854](https://projects.xivo.solutions/issues/854)
- Upstream issue: none


## xivo_deadlock_after_transfer

Fixes a deadlock when hanguping an originate when originate in intermediate state

References:
- Original issue: [XiVO 1846](https://projects.xivo.solutions/issues/1846)
- Upstream issue: none?


## xivo_hangupsource

Adds hangupsource to CEL HANGUP record in some scenarios (blind transfer, unanswered call)

References:
- Original issue: [XiVO 2730](https://projects.xivo.solutions/issues/2730)
- Upstream issue: none

## xivo_ami_refer

Adds new AMI action Refer which simulates SIP REFER message (attended transfer).

References:
- Original issue: [XiVO 2912](https://projects.xivo.solutions/issues/2912)
- Upstream issue: none


## xivo_surrogate_channel_crash

There is possible race condition and crash in function one specific scenario when
normal channel is masquesraded to surrogate channel in function ast_channel_early_bridge
when the function is called by channel during its masquerade to surrogate
channel. Because that scenario always calls ast_channel_early_bridge with
second parameter NULL and there is no channel type which does anything real,
when the second parameter is NULL, tha patch returns from the function
immediately in that case to avoid the race.

References:
- Original issue: [XiVO 3332](https://projects.xivo.solutions/issues/3332)
- Upstream issue: none


## xivo_disable_stun_msg_caching

Adds patch to pjproject 2.10 which disables STUN msg caching

References:
- Original issue: [XiVO 3411](https://projects.xivo.solutions/issues/3411)
- Upstream issue: [pjproject 2505](https://github.com/pjsip/pjproject/issues/2505)

## xivo_opus_download_url

As our asterisk package version is prefixed by '8:', the download script version based
url generation is broken, this patch resolves it.

Observed error without patch:
00:39:32 Installing modules from codecs...
00:39:32 codec_opus: Unable to fetch http://downloads.digium.com/pub/telephony/codec_opus/asterisk-8:16.0/x86-64/manifest.xml

## xivo_queue_ami_ast11_compat

When channel is moved away from a queue, it is replaced by surrogate type
channel to correctly finish its role in queue structures.
Because AMI does not send events about surrogate channels, AMI event
informing about that channel is leaving the queue are missing.
This patch fises the problem and ensures that uniqueid of orighinal channel
is sent in AMI event instead of uniqueid of surrogate channel.
This patch is merge of former patches
kxivo_queue_caller_leave_ast11_compat and xivo_agent_complete_ast11_compat

References:
- Original issue: [XiVO 3874](https://projects.xivo.solutions/issues/3733)

## xivo_fix_turn_crash

Fixes crash in turn protocol probably caused by race between session
destoying and reading and processing new packet in session.
Already fixed in pjproject 2.11, but that version is not used by asterisk
yet. Upstream patch copied to xivo

References:
- Original issue: [XiVO 4330](https://projects.xivo.solutions/issues/4330)
- Upstream issue: [pjproject 2485](https://github.com/pjsip/pjproject/issues/2485)

## xivo_fix_turn_stun_crash

Fixes crash in stun used in turn protocol.
Patch adds test if variable instance in callback function ast_rtp_on_turn_rx_rtp_data
is NULL and retuns immediately, when it is.

References:
- Original issue: [XiVO 5196](https://projects.xivo.solutions/issues/5196)

## xivo_pjsip_autocreate

For PJSIP implements autocreate behavior similar to autocreate in chan_sip.
Module res_pjsip_endpoint_identifier_autocreate.so must be loaded
Writable sorcery storage must be defined in sorcery.conf.
Endpoint [autocreate] must be defined as a template for autocreated
endpoints.
Optional field autocreate_prefix can be defined in [global] section in
pjsip_conf to restrict usernames allowable to use autocrate functionality.
Autocreate function creates endpoint and aor for matchin unknown users to
allow calls and registrations.

References:
- Original issue: [XiVO 5289](https://projects.xivo.solutions/issues/5289)

## xivo_pjsip_origin_address_during_reinvite

Changes way how origin address in SDP is selected to ensure, it will be same
in re-INVITES

References:
- Original issue: [XiVO 5417](https://projects.xivo.solutions/issues/5417)

## xivo_avoid_race_in_rtp

This patch add extra check ensuring ssl structure in RTP instance is not NULL
during processing srtp packet. Patched function originally tests it, but there is
an unlock/lock sequence after the check which allows other threads to
destroy ssl structure after test.

References:

- Original issue: [XiVO 6110](https://projects.xivo.solutions/issues/6110)

## xivo_fix_turn_high_cpu

Fixes high CPU load when TURN server is unreachable.
It is because parameter timespec in ast_cond_timedwait func is set
incorrectly in loop and thread does not sleep at all.
Also waiting for unreachable TURN server block all SIP traffic at least with
chan_sip module. So waiting for TURN server is stopped after 2 secs.

References:
- Original issue: [XiVO 4586](https://projects.xivo.solutions/issues/4586)
- Upstream issue: [ASTERISK-30055](https://issues.asterisk.org/jira/browse/ASTERISK-30055)
