#!/bin/bash

[ -z "$1" ] && echo "You need to provide asterisk version in argument" && exit 1

ASTERISK_VERSION=$1
docker build --build-arg ASTERISK_VERSION="$ASTERISK_VERSION" -f Dockerfile-debug -t xivoxc/asterisk-debug:${ASTERISK_VERSION} ..