#!/bin/sh

# run as user asterisk by default
ASTERISK_USER=${ASTERISK_USER:-asterisk}

ASTERISK_VANILLA_DIR="/usr/src/asterisk-vanilla/"
ASTERISK_XIVO_DIR="/usr/src/asterisk-xivo/"

install_asterisk_bin() {
    local asterisk_src_dir="${1}"; shift
    cd "$asterisk_src_dir"
    make install
    cd -
}

if [ "$1" = "" ] || [ "$1" = "vanilla" ]; then
  install_asterisk_bin "${ASTERISK_VANILLA_DIR}"
  COMMAND="/usr/sbin/asterisk -T -U ${ASTERISK_USER} -p -vvvdf"
elif [ "$1" = "xivo" ]; then
  install_asterisk_bin "${ASTERISK_XIVO_DIR}"
  COMMAND="/usr/sbin/asterisk -T -U ${ASTERISK_USER} -p -vvvdf"
else
  COMMAND="$@"
fi

exec ${COMMAND}