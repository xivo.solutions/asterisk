#!/bin/bash

[ -z "$1" ] && echo "You need to provide asterisk version in 1st argument" && exit 1
[ -z "$2" ] && echo "You need to provide full path towards a directory containing asterisk conf in 2nd argument" && exit 1

AST_VERSION=$1
CONF_DIR=$2
CONF_DIR_NAME=$(basename "$CONF_DIR")
AST_FLAVOR=${3:-vanilla}

docker run -ti \
    --name "asterisk-${AST_VERSION,,}-${CONF_DIR_NAME,,}" \
    -p 5060:5060/udp \
    -p "10000-10009:10000-10009/udp" \
    -v "${PWD}/logs":/var/log/asterisk \
    -v "${CONF_DIR}":/etc/asterisk/ \
    "xivoxc/asterisk-debug:${AST_VERSION}" "${AST_FLAVOR}"